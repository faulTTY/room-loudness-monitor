# Room Loudness, Humidity and Temperature Monitor
## Overview

This Arduino project monitors the average loudness in a room and sends a boolean value to a web server based on a predefined threshold. The data is sent to a specified web server endpoint using HTTP.

## Dependencies

- Arduino
- ESP8266WiFi
- ESP8266HTTPClient
- WiFiClientSecure
- RunningAverage

## Hardware Requirements

- ESP8266 board
- Electret Microphone Amplifier - MAX9814 with Auto Gain Control
- DHT11 temperature & humidity sensor

## Installation

- Clone the repository to your local machine.
- Open the Arduino IDE.
- Install the required libraries listed in the Dependencies section.
- Connect the microphone
    - VDD to Pin 14
    - GND to Pin 12
    - OUT to the analog A0 pin
- Connect the DHT11
    - VDD to Pin 0
    - GND to Pin 5
    - Data to Pin 4
- Connect your ESP8266 board to your computer.
- Open the ambientLoudnessDataCollector.ino file in the Arduino IDE.
- Set up your Wi-Fi credentials and the web server endpoint in the arduino_secrets.h file.
- Upload the code to your ESP8266 board.

## Configuration

In the arduino_secrets.h file, provide your Wi-Fi credentials and the web server endpoint:

```cpp
// arduino_secrets.h

#define SECRET_SSID "your_wifi_ssid"
#define SECRET_PASS "your_wifi_password"
```

## Usage

- Power on the ESP8266 board.
- The device will connect to the specified Wi-Fi network.
- The average loudness in the room will be measured using an analog sensor.
- If the difference between the maximum and minimum loudness exceeds a threshold, a boolean value will be sent to the web server.
- The room humidity and temperature will be measured as well.
- Monitor the serial output for debugging information.

## Contributing

Feel free to contribute to the development of this project by creating issues or submitting pull requests.

## License

This project is licensed under the MIT License - see the LICENSE file for details.