/*
 * Listen to the average loudness in a room. Send a boolean value to a web server
 * depending on a threshold.
 * November 2023
 */

#include "arduino_secrets.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>
#include "RunningAverage.h"

#include <Adafruit_Sensor.h>
#include <DHT.h>
#define DHTTYPE DHT11
#define DHTPIN 4

// Initialize DHT sensor for normal 16mhz Arduino:
DHT dht = DHT(DHTPIN, DHTTYPE);

// Bot credentials
const String host = "http://10.1.10.13:5000/receive_data";

// Global variables and objects
const char ssid[] = SECRET_SSID;
const char pass[] = SECRET_PASS;
const int numberOfMeasurements = 500;
RunningAverage myRA(numberOfMeasurements);
int tempSensorGnd = 5;
int tempSensorVcc = 0;
int micGnd = 12;
int micVcc = 14;

bool isFanOn() {
  myRA.clear(); // explicitly start clean
  float val;
  float myArr[numberOfMeasurements];

  for (int k = 0; k < numberOfMeasurements; k++) {
    val = analogRead(0);
    myRA.addValue(val);
    myArr[k] = val;
    delay(4);
  }

  return (myRA.getMax() - myRA.getMin()) > 200;
}

void sendData(int loudness, float humidity, float temp) {
  WiFiClient client;
  HTTPClient https;

  Serial.println("[HTTPS] begin...");
  https.begin(client, host); // HTTPS
  https.addHeader("Content-Type", "application/json");

  Serial.println("[HTTPS] POST...");
  // Start connection and send HTTP header and body
  String body = "{\"loudness\": " + String(loudness) + ", \"humidity\": " + String(humidity) + ", \"temp\": " + String(temp) + "}";
  int httpCode = https.POST(body);

  // httpCode will be negative on error
  if (httpCode > 0) {
    Serial.printf("[HTTPS] POST... code: %d\n", httpCode);

    // File found at server
    if (httpCode == HTTP_CODE_OK) {
      const String &payload = https.getString();
      Serial.println("Received payload:\n<<");
      Serial.println(payload);
      Serial.println(">>");
    }
  } else {
    Serial.printf("[HTTP] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
  }

  https.end();
}

void setup() {
  // Serial
  Serial.begin(115200);
  Serial.println("\n\n");

  // Microphone
  pinMode(micGnd, OUTPUT);
  pinMode(micVcc, OUTPUT);
  digitalWrite(micGnd, LOW);
  digitalWrite(micVcc, HIGH);

  // Temp Humidity Sensor
  pinMode(tempSensorGnd, OUTPUT);
  pinMode(tempSensorVcc, OUTPUT);
  digitalWrite(tempSensorGnd, LOW);
  digitalWrite(tempSensorVcc, HIGH);
  dht.begin();

  // Wifi
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nConnected! IP address: " + WiFi.localIP().toString());
}

void loop() {
  // Wait for WiFi connection
  if (WiFi.status() == WL_CONNECTED) {
    // Get data
    int loudness = isFanOn() ? 1 : 0;
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    // Print data
    Serial.print("Loudness: ");
    Serial.println(loudness);
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.println(" %");
    Serial.print("Temperat: ");
    Serial.print(t);
    Serial.println(" C");
    
    // Send data
    sendData(loudness, h, t);
  }

  delay(500);
}
